import { MunsiPage } from './munsi.po';

describe('Munsi', () => {
  let page: MunsiPage;

  beforeEach(() => {
    page = new MunsiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getMainHeading()).toEqual('Hello, world!');
  });
});
