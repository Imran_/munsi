import { browser, by, element } from 'protractor';

export class MunsiPage {
  navigateTo() {
    return browser.get('/');
  }

  getMainHeading() {
    return element(by.css('munsi-root h1')).getText();
  }
}
