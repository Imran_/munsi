﻿using Munsi.Infrastructure;
using Microsoft.EntityFrameworkCore;
using SaasKit.Multitenancy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Munsi.Models
{
    public class MunsiTenantDbContext : MunsiDbContext<MunsiTenantUser,MunsiTenantRole>
    {
        TeanantConfiguration _teanantConfiguration;
        ITenant tenant;
        public MunsiTenantDbContext(DbContextOptions dBContextOptions, TenantContext<ITenant> resolver, TeanantConfiguration confuring):base(dBContextOptions)
        {
            _teanantConfiguration = confuring;
            if (resolver != null)
            {
                this.tenant = resolver.Tenant;
                Database.EnsureCreated();
            }
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            _teanantConfiguration._confuring.Invoke(optionsBuilder,tenant);
        }
    }

    public sealed class TeanantConfiguration
    {
        public Action<DbContextOptionsBuilder, ITenant> _confuring;
        
    } 
}
