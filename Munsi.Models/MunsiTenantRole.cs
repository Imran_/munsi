﻿using Munsi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Munsi.Models
{
    public class MunsiTenantRole :MunsiRole
    {
        public MunsiTenantRole()
        {

        }

        public MunsiTenantRole(string Role,string Description):base(Role,Description)
        {

        }
    }
}
