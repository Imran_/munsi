﻿Scaffolding has generated all the files and added the required dependencies.

However the Munsilication's Startup code may required additional changes for things to work end to end.
Add the following code to the Configure method in your Munsilication's Startup class if not already done:

        munsi.UseMvc(routes =>
        {
          routes.MapRoute(
            name : "areas",
            template : "{area:exists}/{controller=Home}/{action=Index}/{id?}"
          );
        });
