﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Munsi.Infrastructure
{
    public sealed class MunsiPermission
    {
        public MunsiPermission()
        {

        }
        public MunsiPermission(string name, string value, string groupName, string description = null)
        {
            Name = name;
            Value = value;
            GroupName = groupName;
            Description = description;
        }
        public string Name { get; set; }
        public string Value { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Value;
        }


        public static implicit operator string(MunsiPermission permission)
        {
            return permission.Value;
        }
    }

    public static class MunsilicationPermissions
    {
        public static ReadOnlyCollection<MunsiPermission> AllPermissions;


        public const string UsersPermissionGroupName = "User Permissions";
        public static MunsiPermission ViewUsers = new MunsiPermission("View Users", "users.view", UsersPermissionGroupName, "Permission to view other users account details");
        public static MunsiPermission ManageUsers = new MunsiPermission("Manage Users", "users.manage", UsersPermissionGroupName, "Permission to create, delete and modify other users account details");

        public const string RolesPermissionGroupName = "Role Permissions";
        public static MunsiPermission ViewRoles = new MunsiPermission("View Roles", "roles.view", RolesPermissionGroupName, "Permission to view available roles");
        public static MunsiPermission ManageRoles = new MunsiPermission("Manage Roles", "roles.manage", RolesPermissionGroupName, "Permission to create, delete and modify roles");
        public static MunsiPermission AssignRoles = new MunsiPermission("Assign Roles", "roles.assign", RolesPermissionGroupName, "Permission to assign roles to users");


        static MunsilicationPermissions()
        {
            List<MunsiPermission> allPermissions = new List<MunsiPermission>()
            {
                ViewUsers,
                ManageUsers,

                ViewRoles,
                ManageRoles,
                AssignRoles
            };

            AllPermissions = allPermissions.AsReadOnly();
        }

        public static MunsiPermission GetPermissionByName(string permissionName)
        {
            return AllPermissions.Where(p => p.Name == permissionName).SingleOrDefault();
        }

        public static MunsiPermission GetPermissionByValue(string permissionValue)
        {
            return AllPermissions.Where(p => p.Value == permissionValue).SingleOrDefault();
        }

        public static string[] GetAllPermissionValues()
        {
            return AllPermissions.Select(p => p.Value).ToArray();
        }

        public static string[] GetAdministrativePermissionValues()
        {
            return new string[] { ManageUsers, ManageRoles, AssignRoles };
        }
    }
}
