﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Munsi.Infrastructure
{
    public class MunsiDbContext<Tu,Tr> : IdentityDbContext<Tu, Tr, string> where Tu : MunsiUser where Tr : MunsiRole
    {
        public MunsiDbContext(DbContextOptions options) : base(options)
        { }
    }
}
