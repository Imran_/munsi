﻿using System;
using Munsi.Master.Models;
using Munsi.Models;
using Microsoft.EntityFrameworkCore;
using Munsi.Tenant.Infrastucture;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddMultiTanancyUsingDataBase(this IServiceCollection service)
        {
            service.AddMultitenancy<MasterTenent, TenantResolverUsingDataBase>();
            return service;
        }

        public static IServiceCollection AddTenantMasterSqlDataBase(this IServiceCollection service,Action<DbContextOptionsBuilder> options = null)
        {
            service.AddDbContext<TenantMasterDbContext>(options);
            return service;
        }
        public static IServiceCollection AddTenantSqlDataBase(this IServiceCollection service, Action<DbContextOptionsBuilder> options = null)
        {
            service.AddDbContext<TenantMasterDbContext>(options);
            return service;
        }

        public static IServiceCollection AddDataSqlDataBase(this IServiceCollection service, Action<DbContextOptionsBuilder> options = null)
        {
            service.AddDbContext<MunsiTenantDbContext>(options);
            return service;
        }

        public static IServiceCollection AddTenantConfiguration(this IServiceCollection service, TeanantConfiguration teanantConfiguration)
        {
            service.AddScoped(typeof(TeanantConfiguration),o=>teanantConfiguration );
            return service;
        }
    }
}
