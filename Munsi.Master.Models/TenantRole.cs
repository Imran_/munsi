﻿using Munsi.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Munsi.Master.Models
{
    public class TenantRole: MunsiRole
    {
        public TenantRole()
        {

        }
        public TenantRole(string rolename,string description):base(rolename,description)
        {

        }
    }
}
