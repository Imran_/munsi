﻿using Munsi.Infrastructure;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Munsi.Master.Models
{
    public class TenantMasterDbContext : MunsiDbContext<TenantUser,TenantRole>
    {
        public TenantMasterDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<MasterTenent> Tenants { get; set; }
    }
}
